package com.evgeny.goncharov.onlinedictionary.ui.fragment;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.evgeny.goncharov.onlinedictionary.R;
import com.evgeny.goncharov.onlinedictionary.mvp.presenter.HistoryPresenter;
import com.evgeny.goncharov.onlinedictionary.mvp.view.HistoryView;

/**
 * Created by Evgeny Goncharov on 30.10.2018.
 * jtgn@yandex.ru
 */

public class HistoryFragment extends BaseFragment implements HistoryView {

    @InjectPresenter
    HistoryPresenter presenter;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    @LayoutRes
    @Override
    protected int getContentRes() {
        return R.layout.history_fragment;
    }


}
