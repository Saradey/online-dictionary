package com.evgeny.goncharov.onlinedictionary;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Evgeny Goncharov on 30.10.2018.
 * jtgn@yandex.ru
 */

public class MainApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);

        RealmConfiguration realmConfiguration = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(realmConfiguration);
    }


}
