package com.evgeny.goncharov.onlinedictionary.ui.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.evgeny.goncharov.onlinedictionary.R;
import com.evgeny.goncharov.onlinedictionary.common.PagerAdapterForFragment;
import com.evgeny.goncharov.onlinedictionary.mvp.presenter.MainPresenter;
import com.evgeny.goncharov.onlinedictionary.mvp.view.MainView;
import com.evgeny.goncharov.onlinedictionary.ui.fragment.HistoryFragment;
import com.evgeny.goncharov.onlinedictionary.ui.fragment.SettingsFragment;
import com.evgeny.goncharov.onlinedictionary.ui.fragment.TranslationFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends MvpAppCompatActivity implements MainView {


    @InjectPresenter
    MainPresenter presenter;

    @BindView(R.id.languge_fragment)
    Toolbar toolbar;

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    @BindView(R.id.view_pager)
    ViewPager fieldFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initToolbar();
        initTab();
    }


    private void initToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setVisibility(View.GONE);
    }


    private void initTab() {
        PagerAdapterForFragment pagerAdapter = new PagerAdapterForFragment(getSupportFragmentManager());
        pagerAdapter.setFragment(new TranslationFragment(), getString(R.string.tablayout_menu_1));
        pagerAdapter.setFragment(new HistoryFragment(), getString(R.string.tablayout_menu_2));
        pagerAdapter.setFragment(new SettingsFragment(), getString(R.string.tablayout_menu_3));

        fieldFragment.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(fieldFragment);

        TabLayout.Tab translation = tabLayout.getTabAt(0);
        if (translation != null)
            translation.setIcon(R.drawable.ic_translate);

        TabLayout.Tab history = tabLayout.getTabAt(1);
        if (history != null)
            history.setIcon(R.drawable.ic_history);

        TabLayout.Tab setting = tabLayout.getTabAt(2);
        if (setting != null)
            setting.setIcon(R.drawable.ic_settings);
    }


}
