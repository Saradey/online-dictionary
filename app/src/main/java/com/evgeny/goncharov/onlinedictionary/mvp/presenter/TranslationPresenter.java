package com.evgeny.goncharov.onlinedictionary.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.evgeny.goncharov.onlinedictionary.mvp.view.TranslationView;

/**
 * Created by Evgeny Goncharov on 30.10.2018.
 * jtgn@yandex.ru
 */

@InjectViewState
public class TranslationPresenter extends MvpPresenter<TranslationView> {
}
