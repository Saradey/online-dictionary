package com.evgeny.goncharov.onlinedictionary.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Evgeny Goncharov on 01/11/2018.
 * jtgn@yandex.ru
 */

public class Tr extends RealmObject {

    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("pos")
    @Expose
    private String pos;
    @SerializedName("syn")
    @Expose
    private RealmList<Syn> syn = new RealmList<>();

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public RealmList<Syn> getSyn() {
        return syn;
    }

    public void setSyn(RealmList<Syn> syn) {
        this.syn = syn;
    }

}
