package com.evgeny.goncharov.onlinedictionary.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by Evgeny Goncharov on 01/11/2018.
 * jtgn@yandex.ru
 */

//синонимы.
public class Syn extends RealmObject {

    @SerializedName("text")
    @Expose
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
