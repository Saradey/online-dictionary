package com.evgeny.goncharov.onlinedictionary.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Evgeny Goncharov on 01/11/2018.
 * jtgn@yandex.ru
 */

//тело ответа
public class BodyResponse extends RealmObject {

    @SerializedName("def")
    @Expose
    private RealmList<Def> def = new RealmList<>();

    public RealmList<Def> getDef() {
        return def;
    }

    public void setDef(RealmList<Def> def) {
        this.def = def;
    }

}
