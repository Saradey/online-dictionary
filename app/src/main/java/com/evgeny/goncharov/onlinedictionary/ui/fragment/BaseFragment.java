package com.evgeny.goncharov.onlinedictionary.ui.fragment;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;

/**
 * Created by Evgeny Goncharov on 30.10.2018.
 * jtgn@yandex.ru
 */

public abstract class BaseFragment extends MvpAppCompatFragment {

    @LayoutRes
    protected abstract int getContentRes();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle saveInstance) {
        return inflater.inflate(getContentRes(), container, false);
    }


}
