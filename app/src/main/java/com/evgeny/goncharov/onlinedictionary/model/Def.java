package com.evgeny.goncharov.onlinedictionary.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Evgeny Goncharov on 01/11/2018.
 * jtgn@yandex.ru
 */

//Массив словарных статей. В атрибуте ts может указываться транскрипция искомого слова
public class Def extends RealmObject {

    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("pos")
    @Expose
    private String pos;
    @SerializedName("tr")
    @Expose
    private RealmList<Tr> tr = new RealmList<>();

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public RealmList<Tr> getTr() {
        return tr;
    }

    public void setTr(RealmList<Tr> tr) {
        this.tr = tr;
    }

}
