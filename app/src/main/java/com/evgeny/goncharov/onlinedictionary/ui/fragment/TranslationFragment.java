package com.evgeny.goncharov.onlinedictionary.ui.fragment;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.evgeny.goncharov.onlinedictionary.R;
import com.evgeny.goncharov.onlinedictionary.mvp.presenter.TranslationPresenter;
import com.evgeny.goncharov.onlinedictionary.mvp.view.TranslationView;

/**
 * Created by Evgeny Goncharov on 30.10.2018.
 * jtgn@yandex.ru
 */


public class TranslationFragment extends BaseFragment implements TranslationView {


    @InjectPresenter
    TranslationPresenter presenter;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @LayoutRes
    @Override
    protected int getContentRes() {
        return R.layout.translation_fragment;
    }


}
